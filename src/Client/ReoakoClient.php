<?php

namespace Drupal\reoako\Client;

class ReoakoClient
{
    /**
     * API Domain
     *
     * @var string
     * @config
     */
	private static $default_api_domain = 'https://api.reoako.nz';

    /**
     *  API base path
     *
     * @var string
     * @config
     */
	private static $default_api_base_path = 'api/v1';

    /**
     * The API key that will be used for the service. Can be set on the singleton to take priority over configuration.
     *
     * @var string
     */
	private $apiKey = '';

    private function getApiKey()
    {
        return $this->apiKey;
    }

    public function setApiKey($apiKey)
    {
    	$this->apiKey = $apiKey;
    }

    public function __construct() {
		$this->domain = self::$default_api_domain;
		$this->origin = \Drupal::request()->getSchemeAndHttpHost();
		$this->endpoint = $this->domain . '/' . self::$default_api_base_path;
    }

	public function search($term) {
		$client = \Drupal::httpClient();

		//Check API key exists
		$apiKey = $this->getApiKey();
		if (empty($apiKey)) {
		  throw new \Exception("API key not set");
		}

		//Set HTTP headers
		$headers = array(
		  'Content-Type' => 'application/json',
		  'Authorization' =>  'Token ' . $apiKey,
		  'Origin' => $this->origin,
		  'Accept' => 'application/json',
		);

		//Send HTTP request
		try {
		  $response = $client->get(
		      $this->endpoint . '/entries/?search=' . $term,
		      ['headers' => $headers]
		  );

		  $json = json_decode($response->getBody(), true);
		  return $json;
		} catch (ClientException $error) {
		  // Get the original response
		  $response = $error->getResponse();
		  // Get the info returned from the remote server.
		  $response_info = $response->getBody();

		  try {
		      $json = json_decode($response_info);
		      if ($json->message) {
		          return $json;
		      }
		  } catch (Exception $e) {

		  }

		  return $response_info;
		} catch (ServerException $error) {
		  // Get the original response
		  $response = $error->getResponse();
		  // Get the info returned from the remote server.
		  $response_info = $response->getBody()->getContents();

		  return $response_info;
		}
	}
}
?>