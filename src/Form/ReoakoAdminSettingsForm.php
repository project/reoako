<?php

namespace Drupal\reoako\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;

/**
 * Configure ReoAko settings for this site.
 */
class ReoakoAdminSettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'reoako_admin_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['reoako.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('reoako.settings');

    $form['general'] = [
      '#type' => 'details',
      '#title' => $this->t('General settings'),
      '#open' => TRUE,
    ];

    $form['general']['reoako_api_key'] = [
      '#default_value' => $config->get('api_key'),
      '#description' => $this->t('The API key provided to you by Reoako. Please contact Reoako if you do not have one.'),
      '#maxlength' => 40,
      '#required' => TRUE,
      '#title' => $this->t('API key'),
      '#type' => 'textfield',
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config('reoako.settings');
    $config
      ->set('api_key', $form_state->getValue('reoako_api_key'))
      ->save();

    parent::submitForm($form, $form_state);
  }
}