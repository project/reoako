<?php

namespace Drupal\reoako\Plugin\CKEditorPlugin;

use Drupal\ckeditor\CKEditorPluginBase;
use Drupal\editor\Entity\Editor;

/**
 * Defines the "Reoako Editor" plugin.
 *
 * @CKEditorPlugin(
 *   id = "reoako",
 *   label = @Translation("Reoako Editor"),
 *   module = "ckeditor_reoakoeditor"
 * )
 */
class ReoakoEditor extends CKEditorPluginBase {

  /**
   * {@inheritdoc}
   */
  public function getDependencies(Editor $editor) {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function getLibraries(Editor $editor) {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function isInternal() {
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function getFile() {
    // Provide the Javascript plugin path.
    return drupal_get_path('module', 'reoako') . '/dist/js/reoako-editor.js';
  }

  /**
   * {@inheritdoc}
   */
  public function getButtons() {
    $iconImage = drupal_get_path('module', 'reoako') . '/dist/img/reoako-editor-icon.png';

    return [
      'reoako' => [
        'label' => t('Reoako translation dialog'),
        'image' => $iconImage,
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getConfig(Editor $editor) {
    return [];
  }

}