<?php

namespace Drupal\reoako\Controller;

use Drupal\reoako\Client\ReoakoClient;
use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\HttpFoundation\Response;

class ReoakoController extends ControllerBase {
  /**
   *
   */
  public function search() {
    //Establish search term
    if (!empty(\Drupal::request()->query->get('search_term'))) {
      //Get search term from post data
      $search_term = \Drupal::request()->query->get('search_term');
    } elseif (!empty(\Drupal::request()->request->get('search_term'))) {
      //Get search term from URL
      $search_term = \Drupal::request()->request->get('search_term');
    } else {
      $search_term = "";
    }

    //Get results
    $error = "";
    $data = [];
    if (!empty($search_term)) {
      //Initialize search client
      $reoakoClient = new ReoakoClient();

      //Get API key from Drupal settings and apply to client
      $config = \Drupal::config('reoako.settings');
      $apiKey = $config->get('api_key');
      $reoakoClient->setApiKey($apiKey);

      //Perform search
      $results = $reoakoClient->search($search_term);

      if (!empty($results['error'])) {
        $error = $results->error; 
      } else {
        foreach ($results as $rk => $rv) {
          if ($rk == 'results') {
            foreach ($rv as $e) {
              $r = [
                  'headword' => $e['headword'],
                  'function' => $e['function'],
                  'definition' => $e['definition'],
                  'translations' => [
                      'url' => $e['translations'][0]['url'],
                      'en' =>  $e['translations'][0]['en'],
                      'mi' =>  $e['translations'][0]['mi'],
                      'slug' => $e['translations'][0]['slug'],
                      'audio_url' => $e['translations'][0]['audio_url'],
                  ]
              ];
              $data[] = $r;
            }
          }
        }
      }
    }

    //Set module script directory so iframe return script can be accessed
    $script_directory = '/' . drupal_get_path('module', 'reoako') . '/dist/js';
    
    //Construct output using reoako_dialog theme/template
    $build = [
      '#theme' => 'reoako_dialog',
      '#search_term' => $search_term,
      '#results' => $data,
      '#error' => $error,
      '#script_directory' => $script_directory
    ];

    //Render for output
    $output = \Drupal::service('renderer')->renderRoot($build);

    //Create and return Symfony response to avoid rest of Drupal templating
    $response = new Response();
    $response->setContent($output);
    return $response;
  }

}