# [Reoako](https://www.reoako.nz/)

This is a Drupal 9 module that implements [Reoako](https://www.reoako.nz/). Reoako helps content managers to embrace the use of our national language in Aotearoa. Use te reo Māori correctly, in the right context, and support your readers with word definitions and pronunciation.

## Requirements

* Drupal 9.x
* Reoako API key - [register here for more info](https://www.reoako.nz/)

## Installation

1. This module is still in development and as such is not yet available to install through composer. Instead, manually upload all files to /web/modules/custom/reoako on your Drupal site.

2. Install the module on your Drupal site's Extend page (admin/modules)

3. Set your API key at Configuration > Development > Reoako settings

4. Navigate to Configuration > Content authoring > Text formats and editors. For each desired text format that one would like to enable Reoako for, click the 'Configure' button then drag the "R" icon onto the Active toolbar. Save configuration.

## Usage

Usage is same as Silverstripe.

When editing any node using the built-in CKEditor plugin, the 'R' button allows one to add/edit Reoako words.
