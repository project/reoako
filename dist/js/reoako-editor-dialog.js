function generateIframeHtml(search_term) {
    var html = '<iframe id="reoako-iframe" src="/reoako-modal/search?search_term=' + search_term + '" tabindex="-1" style="width: 800px; height: 400px;"></iframe>';
    return html;
}

CKEDITOR.dialog.add('reoakoDialog', function (editor) {
   return {
        title: 'Reoako Search',
        minWidth: 800,
        minHeight: 400,

        contents: [
            {
                id: 'tab-search',
                label: 'Search',
                elements: [
                    {
                        type: 'html',
                        html: generateIframeHtml(editor.getSelection().getSelectedText()), /* Dummy */
                    },
                ]
            },
        ],

        //Declare empty buttons array to hide OK/Cancel defaults
        buttons: [
            
        ],


        onShow: function() {
            //Get currently selected text from editor (if any selected)
            var selection = editor.getSelection().getSelectedText();

            //Get current element at cursor position 
            var currentElement = editor.getSelection().getStartElement();

            if (currentElement.hasClass('reoako-trigger')) {
                //Get iframe html tag using current reoako tag as search
                var iframeHtml = generateIframeHtml(currentElement.getText());
            } else {
                //Get iframe html tag from selected text as search
                var iframeHtml = generateIframeHtml(selection);
            }
            
            //Update dialog iframe
            var iframeToDelete = document.getElementById('reoako-iframe');
            var iframeToDeleteParent = iframeToDelete.parentNode;
            iframeToDeleteParent.removeChild(iframeToDelete);
            iframeToDeleteParent.innerHTML = iframeHtml + iframeToDeleteParent.innerHTML;
        },
    };
});