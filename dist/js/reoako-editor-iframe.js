/* Retrieve parent CKEditor from inside this iframe */
var parentWindow = window.parent;
var dialog = parentWindow.CKEDITOR.dialog.getCurrent();
var editor = parentWindow.CKEDITOR.dialog.getCurrent().getParentEditor();

/* Ensure we have CKEditor running and then attach click event to words */
if (parentWindow && dialog && editor) {
    parentWindow.jQuery(".choose-word", document).on("click", (e) => {
        /* Obtain variables from clicked element */
        var translation = e.currentTarget.getAttribute("data-reoako-translation");
        var id = e.currentTarget.getAttribute("data-reoako-id");
        var headword = e.currentTarget.getAttribute("data-reoako-headword");
        var word = e.currentTarget.getAttribute("data-reoako-translation");

        /* Construct HTML from entry */
        let constructed_html =
            ' <span data-reoako-headword="' +
            headword +
            '" data-reoako-id="' +
            id +
            '" data-reoako-translation="' +
            translation +
            '" class="reoako-trigger" tabindex="0" role="button" title="Show translation" aria-label="Show translation" lang="mi">' +
            word +
            '</span> ';

        //Get current element at cursor position 
        var currentElement = editor.getSelection().getStartElement();
        if (currentElement.hasClass('reoako-trigger')) {
            //Replace previous reoako element with new one
            currentElement.remove();
            editor.insertHtml(constructed_html);
        } else {
            //Insert new reoako element into editor
            editor.insertHtml(constructed_html);
        }

        /* Close dialog window */
        dialog.hide()
    });
}