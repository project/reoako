CKEDITOR.plugins.add('reoako', {
  init: function(editor) {
    //Set up dialog command
    editor.addCommand('reoako_dialog', new CKEDITOR.dialogCommand('reoakoDialog'));
    CKEDITOR.dialog.add('reoakoDialog', this.path + 'reoako-editor-dialog.js' );

    //Add button to toolbar
    editor.ui.addButton('reoako', {
      label: 'Reoako translation dialog',
      toolbar: 'insert',
      command: 'reoako_dialog',
      icon: this.path + '../img/reoako-editor-icon.png'
    });

    //Set button to active when reoako element selected (and vice versa)
    editor.on('selectionChange', function(evt) {
      var command = editor.getCommand('reoako_dialog'),
      element = evt.data.selection.getStartElement();
      if (element && element.getName() == 'span' && element.hasClass('reoako-trigger')) {
        //Activate button
        command.setState(CKEDITOR.TRISTATE_ON);
      } else {
        //Deactivate button
        command.setState(CKEDITOR.TRISTATE_OFF);
      }
    });

    //Add CSS
    editor.addContentsCss(this.path + '../css/reoako-editor.css' );
  },
});